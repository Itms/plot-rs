extern crate plot;

fn main() {
	match plot::output::output_plot("test.png") {
	    Ok(_) => println!("Success!"),
	    Err(_) => println!("Failure"),
	};
}