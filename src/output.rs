extern crate image;
use self::image::RgbImage;

use std::path::Path;
use std::io::Result;

pub fn output_plot(filename: &str) -> Result<()> {
	let image_buffer = RgbImage::new(50, 50);
	image_buffer.save(Path::new(filename))
}